//
//  ViewController.swift
//  bnetGallery
//
//  Created by Denis Velikanov on 21.09.2021.
//

import UIKit

class ViewController: UIViewController {
    let collectionView = UICollectionView(frame: .init(),
                                          collectionViewLayout: UICollectionViewFlowLayout.init())
    let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout.init()
  
    let dataFetcher = DataFetcher()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        requestData()
        setLayout()
        register()
        setup()
        constraint()
        view.backgroundColor = .systemRed
    }
    
    func setup() {
        view.addSubview(collectionView)
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    func requestData() {
        dataFetcher.request(numberOfPhotoes: 300)
        dataFetcher.dataGroup.notify(queue: DispatchQueue.global()) {
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }
    
    func constraint() {
        collectionView.anchor(top: view.safeAreaLayoutGuide.topAnchor,
                              leading: view.safeAreaLayoutGuide.leadingAnchor,
                              bottom: view.safeAreaLayoutGuide.bottomAnchor,
                              trailing: view.safeAreaLayoutGuide.trailingAnchor)
    }
    
    func register() {
        collectionView.register(MyCell.self,
                                forCellWithReuseIdentifier: MyCell.identifier)
    }


    func setLayout() {
        layout.scrollDirection = .vertical
        layout.itemSize = .init(width: 100, height: 100)
        collectionView.setCollectionViewLayout(layout, animated: true)
    }
    
}

extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataFetcher.imageURLs.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MyCell.identifier,
                                                      for: indexPath)

        if let imageCell = cell as? MyCell {
            var fullUrl = dataFetcher.imageURLs[indexPath.row]
            let _ = fullUrl.removeFirst()
       
            
            imageCell.imageURL = URL(string: fullUrl)
        }
        
        return cell
    }
}

