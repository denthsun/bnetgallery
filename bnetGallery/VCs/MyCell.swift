//
//  MyCell.swift
//  testsCollectionScroll010101
//
//  Created by Denis Velikanov on 21.09.2021.
//

import UIKit
import SDWebImage

class MyCell: UICollectionViewCell {
    
    static let identifier = "MyCell"
    
    let spinner = UIActivityIndicatorView()
    let image = UIImageView()
    
    var imageURL: URL? {
        didSet {
            image.image = nil
            updateUI()
            spinner.startAnimating()
        }
    }
    
    private func updateUI() {
        guard let endUrl = imageURL else { return }
        guard let fullUrl = URL(string:
                                    "http://bnet.i-partner.ru/projects/calc/networkimages\(endUrl)") else { return }
        
        print(fullUrl)
                self.image.sd_setImage(with: fullUrl)
                self.spinner.stopAnimating()
    }
    
    func setupUI() {
        [spinner, image].forEach { contentView.addSubview($0) }
        contentView.backgroundColor = .systemRed
        image.contentMode = .scaleToFill
    }
    
    func constraintUI() {
        spinner.anchor(top: contentView.safeAreaLayoutGuide.topAnchor,
                       leading: contentView.safeAreaLayoutGuide.leadingAnchor,
                       bottom: contentView.safeAreaLayoutGuide.bottomAnchor,
                       trailing: contentView.safeAreaLayoutGuide.trailingAnchor)
        image.anchor(top: contentView.safeAreaLayoutGuide.topAnchor,
                     leading: contentView.safeAreaLayoutGuide.leadingAnchor,
                     bottom: contentView.safeAreaLayoutGuide.bottomAnchor,
                     trailing: contentView.safeAreaLayoutGuide.trailingAnchor)
    }
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        setupUI()
        constraintUI()
        contentView.backgroundColor = .systemPurple
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

