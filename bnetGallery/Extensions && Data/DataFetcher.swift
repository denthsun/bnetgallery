//
//  DataFetcher.swift
//  testsCollectionScroll010101
//
//  Created by Denis Velikanov on 21.09.2021.
//

import Foundation

class DataFetcher {
    var imageURLs: [String] = []
    let dataGroup = DispatchGroup()
    
    func request(numberOfPhotoes: Int) {
        print("IN")
        dataGroup.enter()
        let urlString = "http://bnet.i-partner.ru/projects/calc/networkimages/?a=getRandom&count=\(numberOfPhotoes)"
        guard let url = URL(string: urlString) else { return }
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        print(url)
        
        let dataTask = URLSession.shared.dataTask(with: request) { [weak self] (data, response, error) in
            print(data as Any)
            guard let urls = data else { return }
            let decoder = JSONDecoder()
            let decoded = try? decoder.decode([String].self, from: urls)
            guard let urlsString = decoded else { return }
            self?.imageURLs = urlsString
            print("OUT")
            self?.dataGroup.leave()
            print(urlsString)
        }
        dataTask.resume()
    }
}

